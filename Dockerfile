# escape=`
#Set escape character so that doesn't interfere with Windows file paths
# (must be first line of dockerfile, which is why this comment is second)

####### VERSIONS #######
FROM debian:stretch

LABEL maintainer "chris@lifeid.io"

####### BASIC DEV TOOLS / REQS #######
#Install apt-utils so debconf doesn't complain about configuration for every
# other install
RUN `
  apt-get update `
  && apt-get install -y --no-install-recommends `
      apt-utils `
  && rm -rf /var/lib/apt/lists/*

#Install development tools and gosu for docker privilege fix
RUN `
  apt-get update `
  && apt-get install -y --no-install-recommends `
    apt-transport-https `
    build-essential `
    ca-certificates `
    curl `
    dirmngr `
    git `
    gnupg2 `
    gosu `
    locales `
    man `
    software-properties-common `
    tmux `
    unzip `
    zip `
  && rm -rf /var/lib/apt/lists/*

#Set the locale
RUN `
  echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen `
  && dpkg-reconfigure locales `
  && echo ': "${LANG:=en_US.utf8}"; export LANG' >> /etc/profile

####### Java #######
RUN `
  apt-get update `
  && apt-get install -y --no-install-recommends `
    openjdk-8-jdk-headless `
  && rm -rf /var/lib/apt/lists/*

####### SBT #######
RUN `
  echo "deb https://dl.bintray.com/sbt/debian /" >> /etc/apt/sources.list.d/sbt.list `
  && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823 `
  && apt-get update `
  && apt-get install -y --no-install-recommends `
    sbt `
  && rm -rf /var/lib/apt/lists/*

####### BNFC #######
WORKDIR /tmp
RUN `
  apt-get update `
  && apt-get install -y --no-install-recommends `
    alex `
    cabal-install `
    happy `
  && rm -rf /var/lib/apt/lists/* `
  && git clone --depth=1 https://github.com/BNFC/bnfc.git
WORKDIR /tmp/bnfc/source
RUN `
  cabal sandbox init --sandbox /bnfc `
  && cabal update `
  && cabal install --global

WORKDIR /

####### JFLEX #######
RUN `
  apt-get update `
  && apt-get install -y --no-install-recommends `
    jflex `
  && rm -rf /var/lib/apt/lists/*

####### /etc/hosts #######
# This is a kludge to deal with the fact that running the node on localhost binds to linuxkit-025000000001
RUN `
  echo    '127.0.0.1  linuxkit-025000000001  localhost'  >  /etc/hosts `
  && echo '::1     localhost ip6-localhost ip6-loopback' >> /etc/hosts `
  && echo 'fe00::0 ip6-localnet'                         >> /etc/hosts `
  && echo 'ff00::0 ip6-mcastprefix'                      >> /etc/hosts `
  && echo 'ff02::1 ip6-allnodes'                         >> /etc/hosts `
  && echo 'ff02::2 ip6-allrouters'                       >> /etc/hosts

####### STARTUP #######
ENV PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/bnfc/bin
