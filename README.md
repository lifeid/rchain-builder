# rchain-builder

This repo contains the Dockerfile used to build a Docker container for building the RCHain rnode software. The Docker image is called `lifeid/rchain-builder` and is published on https://hub.docker.com.

## Using lifeid/rchain-builder to build the rchain source code

This docker images is intended to be used mounting the checked out rchain directorying in the container via `docker run` and the `-v` option.

```
git clone git@github.com:rchain/rchain.git
docker run -v ${PWD}/rchain:/rchain -w /rchain lifeid/rchain-builder sbt -Dsbt.log.noformat=true rholang/bnfc:generate node/universal:packageZipTarball
```


## Publishing new Docker images

To publish new versions of this image:
```
git clone git@gitlab.com:lifeid/rchain-builder.git
cd rchain-builder
docker build -t lifeid/rchain-builder .
docker login --username=yourhubusername
docker push
```

## Credits
This Docker file is based on the work done by Jake Gillberg. If you want a Docker container that allows you to run the IDE from the containier chack out his 
https://github.com/Jake-Gillberg/dev-environment